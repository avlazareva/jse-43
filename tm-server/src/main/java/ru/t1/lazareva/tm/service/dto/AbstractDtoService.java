package ru.t1.lazareva.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.api.repository.dto.IDtoRepository;
import ru.t1.lazareva.tm.api.service.IConnectionService;
import ru.t1.lazareva.tm.api.service.dto.IDtoService;
import ru.t1.lazareva.tm.dto.model.AbstractModelDto;
import ru.t1.lazareva.tm.exception.entity.EntityNotFoundException;
import ru.t1.lazareva.tm.exception.field.IdEmptyException;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractDtoService<M extends AbstractModelDto, R extends IDtoRepository<M>> implements IDtoService<M> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractDtoService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    protected EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

    @NotNull
    protected abstract IDtoRepository<M> getRepository(@NotNull final EntityManager entityManager);

    @NotNull
    @Override
    public M add(@NotNull final M model) throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDtoRepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @Override
    public void clear() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDtoRepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return false;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDtoRepository<M> repository = getRepository(entityManager);
            return repository.existsById(id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsByIndex(@Nullable final Integer index) throws Exception {
        if (index == null) return false;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDtoRepository<M> repository = getRepository(entityManager);
            return repository.existsByIndex(index);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<M> findAll() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDtoRepository<M> repository = getRepository(entityManager);
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final Comparator comparator) throws Exception {
        if (comparator == null) return findAll();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDtoRepository<M> repository = getRepository(entityManager);
            return repository.findAll(comparator);
        } finally {
            entityManager.close();
        }
    }


    @Nullable
    @Override
    public M findOneById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable M resultModel;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDtoRepository<M> modelRepository = getRepository(entityManager);
            resultModel = modelRepository.findOneById(id);
        } finally {
            entityManager.close();
        }
        if (resultModel == null) throw new EntityNotFoundException();
        return resultModel;
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final Integer index) throws Exception {
        if (index == null) throw new IdEmptyException();
        @Nullable M resultModel;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDtoRepository<M> modelRepository = getRepository(entityManager);
            resultModel = modelRepository.findOneByIndex(index);
        } finally {
            entityManager.close();
        }
        if (resultModel == null) throw new EntityNotFoundException();
        return resultModel;
    }

    @Override
    public int getSize() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDtoRepository<M> repository = getRepository(entityManager);
            return repository.getSize();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(@Nullable final M model) throws Exception {
        if (model == null) return;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDtoRepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.remove(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(@Nullable final String id) throws Exception {
        @Nullable M result = findOneById(id);
        remove(result);
    }

    @Override
    public void removeByIndex(@Nullable final Integer index) throws Exception {
        @Nullable M result = findOneByIndex(index);
        remove(result);
    }

    @Override
    public M update(@Nullable final M model) throws Exception {
        if (model == null) throw new EntityNotFoundException();
        if (!existsById(model.getId())) throw new EntityNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @Nullable M resultModel;
        try {
            @NotNull final IDtoRepository<M> modelRepository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            resultModel = modelRepository.update(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        if (resultModel == null) throw new EntityNotFoundException();
        return resultModel;

    }

}
